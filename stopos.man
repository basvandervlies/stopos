.\"
.\" Copyright 2013 Willem Vermin, SURFsara
.\"
.\" Licensed under the Apache License, Version 2.0 (the "License");
.\" you may not use this file except in compliance with the License.
.\" You may obtain a copy of the License at
.\"
.\"     http://www.apache.org/licenses/LICENSE-2.0
.\"
.\" Unless required by applicable law or agreed to in writing, software
.\" distributed under the License is distributed on an "AS IS" BASIS,
.\" WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
.\" See the License for the specific language governing permissions and
.\" limitations under the License.
.\"

.TH STOPOS 1 
.SH NAME
stopos \- alias to call the program stoposclient and eval it's output
.SH SYNOPSIS
.B stopos
\fB\ -h,--help  \fP  
.P 8
.B stopos
\fB\ -v,--version  \fP  
.P 8
.B stopos
\fB\ create \fP   [\fB -p,--pool POOL\fP ]
.P 8
.B stopos
\fB\ status \fP   [\fB -p,--pool POOL\fP]
.P 8
.B stopos
\fB\ purge \fP    [\fB -p,--pool POOL\fP ]
.P 8
.B stopos
\fB\ pools \fP
.P 8
.B stopos
\fB\ add   \fP    [\fB -p,--pool POOL\fP ] [\fB FILENAME \fB]
.P 8
.B stopos
\fB\ next  \fP    [\fB -p,--pool POOL\fP ] [\fB -m,--multi \fB]
.P 8
.B stopos
\fB\ remove \fP   [\fB -p,--pool POOL\fP] [\fB KEY \fP ]
.P 8
.B stopos
\fB\ dump \fP     [\fB -p,--pool POOL\fP]
.P 8
The -q,--quiet flag supresses most output.
.P 8
.SH DESCRIPTION
Stopos is an alias using the program stoposclient, an utility to store
and retrieve text lines in a pool.
In general, the
text lines are used as command parameters, see Examples.
.SH COMMANDS
.TP 4
.B -h,--help
Prints usage information.
.TP 4
.B -v,--version
Prints version.
.TP 4
.B create [ -p,--pool POOL ] [ FILENAME ]
Creates new pool.
A pool with the same name will be removed first.
.TP 4
.B status
Prints to standard error       environment variable 
.br
--------------------------------------------------- 
.br
total number of lines added      STOPOS_COUNT
.br 
number of lines present          STOPOS_PRESENT
.br
number of lines never committed  STOPOS_PRESENT0
.TP 4
.TP 4
.B purge [-p,--pool POOL ]
Removes the pool.
.TP 4
.B pools [-p,--pool POOL ]
Lists on standard error the pools available. The environment 
variable STOPOS_VALUE is set accordingly.
.TP 4
.B add [ -p,--pool POOL ] [ FILENAME ]
Adds lines from FILENAME, default from stdin. 
The maximum line length is 1024 characters.
The environment variables STOPOS_RC and STOPS_KEY are set. 
.br
NOTE: when stopos is reading from a pipe as in: 
.br
   cat parmfile | stopos add
.br
no environment variables are set.
.TP 4
.B next [ -p,--pool POOL ] [ -m,--multi ]
Gets the next line from the pool.
The environment variables STOPOS_VALUE, STOPOS_COMMITTED and STOPOS_KEY are set,
see ENVIRONMENT.
By default, the same line in the pool will be
produced only once. When all lines are commited, STOPS_RC will not
be equal to OK. Using the --multi flag, the same line can be produced 
more than once, if necessary stopos will wrap around.
This can be useful for dealing with crashed jobs.
.TP 4
.B remove [-p,--pool POOL ] [ KEY ]
Removes the line with key KEY. 
If not specified on the commandline, the
value of environment variable STOPOS_KEY is used.
.TP 4
.B dump  [-p,--pool POOL ]
Reads the next available line, and puts it,
preceded by it's key and number of commitments in environment variable
STOPOS_VALUE. The first call will produce the first line. 
When all lines have been delivered, STOPOS_RC
gets a value other than OK. A subsequent call will start the dump
with the first line again.
.SH OPTIONS
.TP 4
.B -p,--pool POOL
POOL is a unique name of the pool. 
Default: pool.
The name of the pool can also be set using the environment variable STOPOS_POOL. The command line flag has precedence.

.SH ENVIRONMENT
Stopos sets the following environment variables:
.P
.B STOPOS_RC
if the value is OK, than no errors were found
.P
.B STOPOS_KEY
contains the keyvalue of the line produced with the 'next' command, or
the keyvalue of the line just added. The keyvalue (a short string) is 
guaranteed to be unique between to 'create"s' or between 
'create' and 'purge'
.P
.B STOPOS_COMMITTED
contains the number of times the line has been committed 
.P
.B STOPOS_VALUE
contains the line produced by the 'next' command or the result of the 'pools' command
.P
.B STOPOS_COUNT STOPOS_PRESENT STOPOS_PRESENT0
see the 'status' command above
.P
Stopos uses the following environment variables:
.P
.B STOPOS_POOL 
see the description of the --pool flag under OPTIONS
.P
.B STOPOS_KEY 
see the 'remove' flag above
.P
.B STOPOS_SERVER_URL 
the url stopos uses to access the server, default DEFAULTSERVERURL

.SH EXAMPLES
.TP 4

Create a pool with the first 10 lines from the man page of sed:

 man sed | head > parmfile
 stopos create 
 stopos add parmfile

Get a line:

 stopos next

The environment variable STOPOS_VALUE contains now one of the
first ten lines of the man page of sed.

You can use this in a command like this:

 eval "myparser $STOPOS_VALUE"

The line can be removed by:

 stopos remove

Finally, the pool can be completely removed by:

 stopos purge

.SH FILES
$HOME/.stopos/id
.br
This file is set by stopos to store an unique id, which, together 
with the login name, will be used to identify which pools are yours.
.SH "SEE ALSO"
sara-get-mem-size(1), sara-get-num-cores(1), stoposclient(1), stoposdump(1)
.SH AUTHORS
Willem Vermin
.br
.SH BUGS
No bugs known yet.
