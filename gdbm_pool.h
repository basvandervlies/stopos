
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#ifndef GDBM_POOL_H
#define GDBM_POOL_H
#include <string>
#include "gdbm.h"
#include "stopos_key.h"
#include "stopos_pool.h"
#include "stopos.h"
 
class gdbm_pool : public stopos_pool
{
  private:
    GDBM_FILE dbf;          // handle for access database
    //static const int sync = GDBM_SYNC;
    static const int sync = 0;

    int get_record(std::string &r,const std::string &k,const std::string &l);
    int put_record(const std::string &r,const std::string &k,const std::string &l);
    int remove_record(const std::string &k, const std::string &l);

    std::string key_to_slot(const std::string &k){return "";}

    int ff;

  public:
    // constructor ()
    gdbm_pool();

    // destructor

    ~gdbm_pool();


    int create_db(const std::string &dbname);
    int purge_db(void);

    int open_db(void);
    int close_db(void);
};

#endif
