
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#ifndef WREQUEST_H
#define WREQUEST_H
#include <string>
#include <iostream>
#include <curl/curl.h>
#include "wutils.h"

class messageclass
{
  private:
    std::string protocol;
    std::string whoami;
    std::string pool;
    std::string command;
    std::string multi;
    std::string value;

  public:
  //constructor
    messageclass()
    {
      this->protocol = " ";
      this->whoami   = " ";
      this->pool     = " ";
      this->command  = " ";
      this->multi    = " ";
      this->value    = " ";
    }
    void set_protocol(const std::string &p){ this->protocol = p;    }
    std::string get_protocol(void)         { return this->protocol; }

    void set_whoami(const std::string &u)  { this->whoami = u;      }
    std::string  get_whoami(void)          { return this->whoami;   }

    void set_pool(const std::string &p)    { this->pool = p;        }
    std::string  get_pool(void)            { return this->pool;     }

    void set_command(const std::string &c) { this->command = c;     }
    std::string  get_command(void)         { return this->command;  }

    void set_multi(const std::string &m)   { this->multi = m;       }
    std::string  get_multi(void)           { return this->multi;    }

    void set_value(const std::string &v)   { this->value = v;       }
    std::string  get_value(void)           { return this->value;    }

};

class wrequest
{
  private: 
    CURL *curl;

    std::string configfilename;
    std::string configdirname;
    std::string config;
    void set_configfilename(void);

    std::string serveraddress;
    std::string serverurl;
    int port;

    messageclass message;
    void compose_message(std::string &m);
    int get_answer(void);
    std::string data;
    bool debugging;

  public:
    //constructor
    wrequest(void);

    //destructor
    ~wrequest(void);

    static const unsigned int config_size = 8;

    std::string get_config(void)             { return this->config; };

    int set_serverurl(const std::string &u);
    std::string get_serverurl(void)          { return this->serverurl; }
    
    void set_port(int);
    int  get_port(void);

    int read_config(void);
    int write_config(void);

    void set_protocol(const std::string &p){ this->message.set_protocol(p);       }
    std::string get_protocol(void)         { return this->message.get_protocol(); }

    void set_whoami(const std::string &u)  { this->message.set_whoami(u);         }
    std::string  get_whoami(void)          { return this->message.get_whoami();   }

    void set_pool(const std::string &p)    { this->message.set_pool(p);           }
    std::string  get_pool(void)            { return this->message.get_pool();     }

    void set_command(const std::string &c) { this->message.set_command(c);        }
    std::string  get_command(void)         { return this->message.get_command();  }

    void set_multi(const std::string &m)   { this->message.set_multi(m);          }
    std::string  get_multi(void)           { return this->message.get_multi();    }

    void set_value(const std::string &v)   { this->message.set_value(v);          }
    std::string  get_value(void)           { return this->message.get_value();    }

    void set_debug(void)                   { this->debugging = 1; }
    void unset_debug(void)                 { this->debugging = 0; }

    int send_message(std::string &body,std::string &header);
  
};
#endif
