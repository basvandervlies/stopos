
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <string>
#include "shellenv.h"
//
// Example:
//   shellenv("aap noot mies", "LEESPLANK", SHELL_SH);
// returns the string:
//   LEESPLANK='aap noot mies';export LEESPLANK;
//
//  "'" characters in s are escaped
//

std::string shellenv(std::string s, const std::string name, int shelltype)
{
  std::string r = "'";
  std::string::iterator it;
  for ( it=s.begin() ; it < s.end(); it++ )
  {
    if ( *it == '\'')
    {
      r.push_back('\'');
      r.push_back('\\');
      r.push_back('\'');
      r.push_back('\'');
    }
    else
      r.push_back(*it);
  }
  r.push_back('\'');
  switch (shelltype)
  {
    case SHELL_CSH:
      r = "setenv " + name + " " + r + ";\n";
      break;
    case SHELL_SH: 
      r = name + "=" + r + ";" + "export " + name + ";\n";
      break;
  }
  return r;
}
