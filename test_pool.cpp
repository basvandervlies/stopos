
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include "wutils.h"
#include "clocks.h"
#ifdef MAKE_GDBM
#include "gdbm_pool.h"
#endif
#ifdef MAKE_FLAT
#include "flatfile_pool.h"
#endif
#ifdef MAKE_FILES
#include "files_pool.h"
#endif
#ifdef MAKE_MYSQL
#include "mysql_pool.h"
#endif
#include <unistd.h>

#include "stopos_key.h"
int testkeysetget()
{
  stopos_key k;
  longuint n = k.get<longuint>();
  if (n != 0)
  {
    std::cerr << "Keytesterror new key get: "<< n <<std::endl;
    return 1;
  }

  n = 1234567890123456;
  k.set(n,"");
  longuint l = k.get<longuint>();
  if (l != n)
  {
    std::cerr << "Keytesterror expect: " << n<< ":" << " got:" << l << std::endl;
    return 1;
  }
  return 0;
}
 

int testpool(bool openclose,int which)
{
#define OPENCLOSE \
  if (openclose) \
  {\
     p->close_db();\
     rc = p->open_db();\
     if(rc)\
     {\
        std::cerr << __FILE__<<":"<<__LINE__<<": cannot open db:" << rc << std::endl;\
        return rc;\
     }\
   }

  stopos_pool *p;
  std::string dbname;
  switch (which)
  {
    case 1: dbname = "/tmp/testdb_gdbm";
#ifdef MAKE_GDBM
	    p = new gdbm_pool();
#endif
	    break;
    case 2: dbname = "/tmp/testdb_flat";
#ifdef MAKE_FLAT
	    p = new flatfile_pool();
#endif
	    break;
    case 3: dbname = "/tmp/testdb_dir";
#ifdef MAKE_FILES
	    p = new files_pool();
#endif
	    break;
    case 4: dbname = "testdb";
#ifdef MAKE_MYSQL
	    p = new mysql_pool();
#endif
	    break;
    default: std::cerr << __LINE__ << "INVALID WHICH" << std::endl;
	     return 6;
  }

  std::vector <std::string> lines;
  std::vector <std::string> keys;

  std::string linesfile = "/tmp/linesfile";
  std::string command;
  command="man bash 2>/dev/null | head -n 100  > "+linesfile;
  int rc;
  rc = system(command.c_str());
  if (rc != 0)
    std::cerr << "Iets niet helemaal goed met man ... \n";
  std::ifstream infile;
  infile.open(linesfile.c_str());

  int ntest = 0;
  while (infile)
  {
    ntest++;
    std::string line;
    getline(infile,line);
    lines.push_back(NumberToString(ntest)+":"+line);
  }

  p->set_db_name(dbname);
  rc = p->create_db();
  if (rc !=0)
  {
    std::cerr << __FILE__<<":"<<__LINE__<<": cannot create db:" << rc << std::endl;
    return rc;
  }
  if (which ==0)
  {
    std::cerr << "Ja NU!\n";
    sleep (5);
    std::cerr << "continuing \n";
  }
  rc = p->open_db();
  if (rc !=0)
  {
    std::cerr << __FILE__<<":"<<__LINE__<<": cannot open db\n";
    return 7;
  }

  for (int i=0; i<ntest; i++)
  {
    keys.resize(0);
    int rc;
    std::string key;
    OPENCLOSE
    rc = p->add_line(lines[i],key);
    if (rc!=0) 
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":put_line '"<<lines[i]<<"' number: " << i << ":" << rc << std::endl;
      return rc;
    }
    keys.push_back(key);
  }
  p->close_db();
  rc = p->open_db();
  if (rc !=0)
  {
    std::cerr << __FILE__<<":"<<__LINE__<<": cannot open db\n";
    return rc;
  }
  p->remove_line("4");
  p->remove_line("2");
  p->remove_line("3");
  p->remove_line("1");
  std::string line;
  longuint m;
  std::string getkey;
  p->get_line(line,m,getkey);

  p->create_db();
  rc = p->open_db();
  if (rc !=0)
  {
    std::cerr << __FILE__<<":"<<__LINE__<<": cannot open db\n";
    return rc;
  }

  keys.resize(0);
  for (int i=0; i<ntest; i++)
  {
    int rc;
    std::string key;
    OPENCLOSE
    rc = p->add_line(lines[i],key);
    if (rc!=0) 
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":add_line '"<<lines[i]<<"' number: " << i << std::endl;
      return rc;
    }
    keys.push_back(key);
  }

  // try to read all lines:
  p->close_db();
  delete p;
  switch (which)
  {
#ifdef MAKE_GDBM
    case 1: p = new gdbm_pool();
	    break;
#endif
#ifdef MAKE_FLAT
    case 2: p = new flatfile_pool();
	    break;
#endif
#ifdef MAKE_FILES
    case 3: p = new files_pool();
	    break;
#endif
#ifdef MAKE_MYSQL
    case 4: p = new mysql_pool();
	    break;
#endif
    default: std::cerr << __LINE__ << "INVALID WHICH" << std::endl;
	     return 6;
  }
  p->set_db_name(dbname);
  rc = p->open_db();
  if (rc !=0)
  {
    std::cerr << __FILE__<<":"<<__LINE__<<": cannot open db:"<<rc<<"\n";
    return rc;
  }

  for (int i=0; i<ntest; i++)
  {
    std::string line,getkey;
    int rc;
    longuint m;
    OPENCLOSE
    rc = p->get_line(line,m,getkey);

    if (rc!=0) 
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line:"<<i<<std::endl;
      return rc;
    }
    if (m != 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: 0 got:" << m <<std::endl;
      return 9;
    }
    if (line != lines[i])
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: '"<<lines[i]<<"' got:'" << line <<"'"<<std::endl;
      return 9;
    }
  }

  // test if no further lines can be read
  for (int i=0; i<10; i++)
  {
    std::string line,getkey;
    int rc;
    longuint m;
    OPENCLOSE
    rc = p->get_line(line,m,getkey);
    if (rc == 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: not 0 got:" << rc <<std::endl;
      return 9;
    }
  }
  p->close_db();
  rc = p->open_db();
  if (rc !=0)
  {
    std::cerr << __FILE__<<":"<<__LINE__<<": cannot open db\n";
    return rc;
  }

  for (int i=0; i<ntest; i++)
  {
    int rc;
    OPENCLOSE
    rc = p->remove_line(keys[i]);
    if (rc != 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":cannot remove key " << "'"<< keys[i]<<"'" <<std::endl;
      return 9;
    }
  }
  p->close_db();
  rc = p->open_db();
  if (rc !=0)
  {
    std::cerr << __FILE__<<":"<<__LINE__<<": cannot open db\n";
    return rc;
  }
  for (int i=0; i<ntest; i++)
  {
    int rc;
    OPENCLOSE
    rc = p->remove_line(keys[i]);
    if (rc == 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":can remove removed key " << "'"<< keys[i]<<"'" <<std::endl;
      return 9;
    }
  }
  p->close_db();
  p->open_db();
  p->set_kvp(0);

  // typical use: fill database with lines,
  // read and remove

  {
    for (int i=0; i<ntest; i++)
    {
      int rc;
      std::string addkey;
      OPENCLOSE
      rc = p->add_line(lines[i],addkey);
      if (rc!=0) 
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":put_line '"<<lines[i]<<"' number: " << i << ":"<<rc<<std::endl;
	return rc;
      }
    }
    for (int i=0; i<ntest; i++)
    {
      std::string line,getkey;
      int rc;
      longuint m;
      OPENCLOSE
      rc = p->get_line(line,m,getkey);
      if (rc!=0) 
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line\n";
	return rc;
      }
      if (m != 0)
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: 0 got:" << m <<std::endl;
	return 9;
      }
      if (line != lines[i])
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: '"<<lines[i]<<"' got:'" << line <<"'"<<std::endl;
	return 9;
      }
      OPENCLOSE
      p->remove_line(getkey);
      if (rc !=0)
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":cannot remove key " << "'"<< keys[i]<<"'" <<std::endl;
	return 9;
      }
    }
    // test if no further lines can be read
    for (int i=0; i<10; i++)
    {
      std::string line,getkey;
      int rc;
      longuint m;
      OPENCLOSE
      rc = p->get_line(line,m,getkey);
      if (rc == 0)
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: not 0 got:" << rc <<std::endl;
	return 9;
      }
    }
  }
  p->close_db();
  p->open_db();
  p->set_kvp(1);
  keys.resize(0);
  // kvp test
  for (int i=0; i<ntest; i++)
  {
    int rc;
    std::string key;
    OPENCLOSE
    rc = p->add_line(lines[i],key);
    if (rc!=0) 
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":put_line '"<<lines[i]<<"' number: " << i << std::endl;
      return rc;
    }
    keys.push_back(key);
  }
  for (int i=0; i<ntest*3; i++)
  {
    std::string line,getkey;
    int rc;
    longuint m;
    OPENCLOSE
    rc = p->get_line(line,m,getkey);
    if (rc!=0) 
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line\n";
      return rc;
    }
    if (m != (longuint)i/ntest)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: "<<i/ntest<<" got:" << m <<std::endl;
      return 9;
    }
    if (line != lines[i%ntest])
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: '"<<lines[i]<<"' got:'" << line <<"'"<<std::endl;
      return 9;
    }
  }

  p->close_db();
  p->create_db();
  p->open_db();

  for (int i=0; i<10; i++)
  {
    std::string line,getkey;
    int rc;
    longuint m;
    OPENCLOSE
    rc = p->get_line(line,m,getkey);
    if (rc == 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: not 0 got:" << rc << " line: '" << line << "'" <<std::endl;
      return 9;
    }
  }

  {
    // test remove lines
    p->set_kvp(0);
    keys.resize(0);
    for (int i=0; i<ntest; i++)
    {
      int rc;
      std::string key;
      OPENCLOSE
      rc = p->add_line(lines[i],key);
      if (rc!=0) 
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":put_line '"<<lines[i]<<"' number: " << i << std::endl;
	return rc;
      }
      keys.push_back(key);
    }
    //remove half of lines
    for (int i=0; i<ntest; i+=2)
    {
      OPENCLOSE
      int rc = p->remove_line(keys[i]);
      if (rc !=0)
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":cannot remove key " << "'"<< keys[i]<<"'" <<std::endl;
	return 9;
      }
    }
    // try to get other half
    for (int i=1; i<ntest; i+=2)
    {
      int rc;
      longuint m;
      std::string line,getkey;
      OPENCLOSE
      rc = p->get_line(line,m,getkey);
      if (rc!=0) 
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line\n";
	return rc;
      }
      if (m != 0)
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: 0 got:" << m <<std::endl;
	return 9;
      }
      if (line != lines[i])
      {
	p->close_db();
	std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: '"<<lines[i]<<"' got:'" << line <<"'"<<std::endl;
	return 9;
      }
    }
    // there should be no lines left, check:
    int nrem = 0;
    longuint m; 
    std::string line,getkey;
    while (p->get_line(line,m,getkey)==0)
      nrem++;

    if (nrem != 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: not 0 got:" << nrem << " line: '" << line << "'" <<std::endl;
      return 9;
    }
  }
  p->close_db();
  p->create_db();
  p->open_db();
  keys.resize(0);

  {
    // test the following
    // after an add_line, the next line to be retrieved should be
    // that line, unless there were already other lines not 
    // retrieved yet
    std::string line,key,getkey;
    int rc;
    for (int i=0; i<ntest/2; i++)
    {
      OPENCLOSE
      p->add_line(lines[i],key);
      keys.push_back(key);
    }
    longuint m;
    int l=2;
    for (int i=0; i<l; i++)
    {
      OPENCLOSE
      p->get_line(line,m,getkey);
    }
    // the next line to be retrieved should be lines[l]
    // first, we add a new line
      OPENCLOSE
    p->add_line(lines[ntest/2],key);
    keys.push_back(key);
    //check now which line we get:
      OPENCLOSE
    p->get_line(line,m,getkey);
    if (line != lines[l])
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: '"<<lines[l]<<"' got:'" << line <<"'"<<std::endl;
      return 9;
    }
    // now retrieve all lines and add a new one. Can we retrieve that:
    while (p->get_line(line,m,getkey) == 0);
      OPENCLOSE
    p->add_line(lines[0],key);
      OPENCLOSE
    rc = p->get_line(line,m,getkey);
    if (rc != 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line\n";
      return rc;
    }
    if (line != lines[0])
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: '"<<lines[0]<<"' got:'" << line <<"'"<<std::endl;
      return 9;
    }
  }

  p->create_db();
  p->open_db();
  {
    std::string key,line,getkey;
    longuint m;
    int rc;
    // some delete and insert tests
    // put only one line in the database, and remove it
    OPENCLOSE
    rc = p->add_line(lines[0],key);
    if (rc != 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":add_line\n";
      return rc;
    }

    OPENCLOSE
    rc = p->remove_line(key);
    if (rc != 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":remove_line\n";
      return rc;
    }
    OPENCLOSE
    rc = p->get_line(line,m,getkey);
    if (rc == 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line" <<
	"expected !0, got:"<<rc <<"\n";
      return rc;
    }
    // add two lines, remove last one, we expect that get_line
    // gets the first
    OPENCLOSE
    p->add_line(lines[0],key);
    OPENCLOSE
    p->add_line(lines[1],key);
    OPENCLOSE
    rc = p->remove_line(key);
    if (rc != 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":remove_line\n";
      return rc;
    }
    OPENCLOSE
    rc = p->get_line(line,m,getkey);
    if (rc != 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line\n";
      return rc;
    }
    if (line != lines[0])
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: '"<<lines[0]<<"' got:'" << line <<"'"<<std::endl;
      return 9;
    }
    // add two lines, remove first one, we expect that get_line
    // gets the last
    p->close_db();
    p->create_db();
    p->open_db();
    std::string key0;
    OPENCLOSE
    p->add_line(lines[0],key0);
    OPENCLOSE
    p->add_line(lines[1],key);
    rc = p->remove_line(key0);
    if (rc != 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":remove_line\n";
      return rc;
    }
    OPENCLOSE
    rc = p->get_line(line,m,getkey);
    if (rc != 0)
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line\n";
      return rc;
    }
    if (line != lines[1])
    {
      p->close_db();
      std::cerr << "Error in "<<__FILE__<<":"<<__LINE__<<":get_line expected: '"<<lines[0]<<"' got:'" << line <<"'"<<std::endl;
      return 9;
    }

    // without testing: add ntest lines
    for (int i=0; i<ntest; i++)
    {
      p->add_line(lines[i],key0);
    }

  }

  p->close_db();
  return 0;
}
int main()
{
  double t0;
  bool openclose = 1;
  std::cerr << "running keytest .... ";
  if (testkeysetget()==0)
    std::cerr << "ok\n";
  else
  {
    std::cerr << "NOT OK\n";
  }

#ifdef MAKE_GDBM
  std::cerr << "running gdbm ....... ";
  t0=wallclock();
  if (testpool(openclose,1) == 0)
    std::cerr << "ok " << wallclock()-t0 << std::endl;
  else
  {
    std::cerr << "NOT OK\n";
  }
#endif
#ifdef MAKE_FLAT
  std::cerr << "running flatfile ... ";
  t0=wallclock();
  if (testpool(openclose,2) == 0)
    std::cerr << "ok " << wallclock()-t0 << std::endl;
  else
  {
    std::cerr << "NOT OK\n";
  }
#endif
#ifdef MAKE_FILES
  std::cerr << "running files ...... ";
  t0=wallclock();
  if (testpool(openclose,3) == 0)
    std::cerr << "ok " << wallclock()-t0 << std::endl;
  else
  {
    std::cerr << "NOT OK\n";
  }
#endif
#ifdef MAKE_MYSQL
  std::cerr << "running mysql ...... ";
  t0=wallclock();
  if (testpool(openclose,4) == 0)
    std::cerr << "ok " << wallclock() -t0 << std::endl;
  else
  {
    std::cerr << "NOT OK\n";
  }
#endif
  std::cerr << "all is well that ends well" << std::endl;
  return 0;
}

